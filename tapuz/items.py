# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field


class ForumMessageItem(Item):
    id = Field()
    forum = Field()
    parent = Field()
    thread = Field()
    link = Field()
    title = Field()
    author_id = Field()
    author_name = Field()
    datetime = Field()
    responses = Field()
    reads = Field()
    content = Field()
    origin = Field()
    raw_html = Field()
