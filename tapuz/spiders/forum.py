# -*- coding: utf-8 -*-
from scrapy import Spider, Request
from scrapy.http import HtmlResponse
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Join, MapCompose
from datetime import datetime

from tapuz.items import ForumMessageItem


class ForumSpider(Spider):
    name = "forum"
    allowed_domains = ["tapuz.co.il"]
    start_urls = (
        'http://www.tapuz.co.il/forums/forumpage/{}/',
        'http://www.tapuz.co.il/forums/archive/{}/',
    )

    thread_list_xpath = '//div[@data-parent-message="0"]'
    message_list_xpath = '//div[@data-parent-message]'
    item_fields = {
        'id': './@id',
        'parent': './@data-parent-message',
        'link': './/a[contains(@class, "msg-bullet")]/@href',
        'title': './/div[contains(@class, "msg-subject")]/node()',
        'author_id': './/div[contains(@class, "msg-user-link")]/@id',
        'author_name': './/div[contains(@class, "msg-user-link")]/text()',
        'datetime': './/div[contains(@class, "msg-date")]/text()',
        'responses': './/div[contains(@class, "responses-count")]/text()',
        'reads': './/div[contains(@class, "message-read-counter")]/text()',
        'content': './/div[contains(@class, "textContent")]/node()',
        'raw_html': '.',
    }

    def __init__(self, forum=0, *args, **kwargs):
        super(ForumSpider, self).__init__(*args, **kwargs)
        self.forum = int(forum)
        self.start_urls = [x.format(forum) for x in self.start_urls]

    def parse(self, response):
        """
        :type response: C{HtmlResponse}
        """
        for thread in response.xpath(self.thread_list_xpath):
            response.meta['thread'] = 0
            result = self._get_result(thread, response)
            yield result
            meta = {
                'forum': self.forum,
                'thread': result['id'],
            }
            yield Request(result['link'], self.parse_thread, meta=meta)

        next_page = response.css(
            '.Pager a[rel="next"]:not(.disabled)::attr(href)').extract_first()
        if next_page:
            url = response.urljoin(next_page)
            yield Request(url, self.parse)

    def parse_thread(self, response):
        """
        :type response: C{HtmlResponse}
        """
        for message in response.xpath(self.message_list_xpath):
            yield self._get_result(message, response)

    def _get_result(self, selector, response):
        """
        :type response: C{HtmlResponse}
        """
        loader = ItemLoader(ForumMessageItem(), selector=selector)
        loader.default_input_processor = MapCompose(unicode.strip)
        loader.default_output_processor = Join()

        for field, xpath in self.item_fields.iteritems():
            loader.add_xpath(field, xpath)
        result = loader.load_item()

        if 'id' in result:
            result['id'] = int(result['id'].split('_')[1])
        if 'parent' in result:
            result['parent'] = int(result['parent'])
        if 'author_id' in result:
            result['author_id'] = int(result['author_id'].split('_')[1])
        if 'datetime' in result:
            result['datetime'] = datetime.strptime(result['datetime'], '%d/%m/%Y %H:%M')
        if 'responses' in result:
            result['responses'] = int(result['responses'])
        if 'reads' in result:
            result['reads'] = int(result['reads'])
        if 'link' in result:
            result['link'] = response.urljoin(result['link'])

        result['origin'] = response.url
        result['forum'] = self.forum
        result['thread'] = response.meta['thread'] or result['id']

        return result

