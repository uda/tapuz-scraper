# -*- coding: utf-8 -*-
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import IntegrityError

from .models import Author, Message, db_connect, create_tables

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class ForumPipeline(object):
    def __init__(self):
        engine = db_connect()
        create_tables(engine)
        self.Session = sessionmaker(bind=engine)
        self.authors = []

    def process_item(self, item, spider):
        session = self.Session()

        author = Author(**{'id': item['author_id'], 'name': item['author_name']})
        if item['author_id'] not in self.authors:
            try:
                session.add(author)
                session.commit()
            except IntegrityError:
                session.rollback()
                self.authors.append(author.id)

        message_item = item.copy()
        del message_item['author_name']
        message = Message(**message_item)
        try:
            session.add(message)
            session.commit()
        except IntegrityError:
            session.rollback()
        except:
            session.rollback()
            raise
        finally:
            session.close()

        return item
