from sqlalchemy import Column, Integer, String, Text, DateTime, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL

import settings

DeclarativeBase = declarative_base()


def db_connect():
    return create_engine(URL(**settings.DATABASE))


def create_tables(engine):
    DeclarativeBase.metadata.create_all(engine)


class Author(DeclarativeBase):
    __tablename__ = 'author'

    id = Column(Integer, primary_key=True)
    name = Column('name', String)


class Message(DeclarativeBase):
    __tablename__ = 'message'

    id = Column(Integer, primary_key=True)
    forum = Column('forum', Integer, index=True)
    parent = Column('parent', Integer, index=True, default=0)
    thread = Column('thread', Integer, index=True, default=0)
    link = Column('link', String)
    title = Column('title', String, index=True)
    author_id = Column('author', Integer, index=True)
    datetime = Column('datetime', DateTime, index=True)
    responses = Column('responses', Integer, default=0)
    reads = Column('reads', Integer, default=0, index=True)
    content = Column('content', Text, default='')
    origin = Column('origin', String)
    raw_html = Column('raw_html', Text)


class Link(DeclarativeBase):
    __tablename__ = 'link'

    id = Column(Integer, primary_key=True)
    message_id = Column('message', Integer, index=True)
    url = Column('url', String, index=True)
    title = Column('title', String, nullable=True)
