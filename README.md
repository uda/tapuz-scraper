# Tapuz Content Scraper

An initial project to scrape tapuz.co.il for a forum's contents.
I used it to save the contents of a forum I took part, that is being archived.

## Notification

This is intended for learning or mild usage, not to make heavy requests against
Tapuz's servers.

Using this and the resulting data is bound to the TOS of Tapuz.
At the time of writing: [Takanon](http://www.tapuz.co.il/general/takanon.asp)

## Install

* Clone the repo
* Create a python 2.7.x virtualenv
* Install using pip (`pip install .`)

## Configure

Change `tapuz/settings.py` file to meet your needs, the run:
`scrapy crawl tapuz -a forum=N` (where N is the relevant forum ID).

## License

See LICENSE file

## TODO

* Attached links insert on scraping
* Get attached files on scraping (per message)
