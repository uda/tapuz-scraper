from setuptools import setup

setup(
    name='TapuzScraper',
    version='0.1dev',
    author='Yehuda Deutsch',
    author_email='yeh@uda.co.il',
    description='A simple scraper for Tapuz Forums',
    license='MIT',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Framework :: Scrapy',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content :: Message Boards',
        'Topic :: Internet :: WWW/HTTP :: Indexing/Search',
    ],
    install_requires=[
        'Scrapy>=1.0.0',
        'SQLAlchemy>=1.0.0',
    ]
)
